<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Inventaris</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="beranda.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Inventaris 
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Administrator<i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="img/kkk.jpeg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Administrator</p>

                            <a href="#"></i>Selamat Datang</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                         <li class="active">
                            <a href="index.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bar-chart-o"></i>
                                <span>Master Data</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="ruang.php"><i class="fa  fa-circle-o"></i>Ruang</a></li>
                                <li><a href="level.php"><i class="fa  fa-circle-o"></i>Level</a></li>
                                <li><a href="jenis.php"><i class="fa  fa-circle-o"></i>Jenis</a></li>
                                <li><a href="pegawai.php"><i class="fa  fa-circle-o"></i>Pegawai</a></li>
								<li><a href="inventaris.php"><i class="fa  fa-circle-o"></i>Inventaris</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-list-ul"></i>
                                <span>Lainnya</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="petugas.php"><i class="fa fa-circle-o"></i>Petugas</a></li>
                                <li><a href="peminjaman.php"><i class="fa fa-circle-o"></i>Peminjaman</a></li>
                                <li><a href="pengembalian.php"><i class="fa fa-circle-o"></i>Pengembalian</a></li>
                                <li><a href="laporan.php"><i class="fa fa-circle-o"></i>Laporan</a></li>
                            </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="box-header">
                        <h3 class="box-title"> Data Jenis</h3>                                    
                    </div><!-- /.box-header -->
					<p align="right"><a href="input_jenis.php"><button type="button" class="btn btn-outline btn-primary fa fa-plus"> Tambah Data </button>
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kode Jenis</th>
                                                <th>Nama Jenis</th>
                                                <th>Keterangan</th>
												<th> Status </th>
                                            </tr>
                                        </thead>
                                        <tbody>
								<?php
            $servername = "127.0.0.1";
            $username = "zahra2";
            $password = "123";
            $dbname = "ujikom";
 
            // Membuat Koneksi
            $koneksi = new mysqli($servername, $username, $password, $dbname);
            
            // Melakukan Cek Koneksi
            if ($koneksi->connect_error) {
                die("Koneksi Gagal : " . $koneksi->connect_error);
            } 
 
            //Melakukan query
            $sql = "SELECT * FROM jenis";
			$hasil = $koneksi->query($sql);
            $no = 1;
            if ($hasil->num_rows > 0) {
                foreach ($hasil as $row) { 
				?>
                  <tr>     
                  <td><?php echo $no; ?></td>
                  <td><?php echo $row['kode_jenis']; ?></td>
				  <td><?php echo $row['nama_jenis']; ?></td>
				  <td><?php echo $row['keterangan']; ?></td>
				  <td>
				  <a href="update_jenis.php?id_jenis=<?php echo $row['id_jenis']; ?>"><button type="button" class="btn btn-primary"> Update</button></a>
				  <a href="hapus_jenis.php?id_jenis=<?php echo $row['id_jenis']; ?>"><button type="button" class="btn btn-danger"> Hapus</button></a>
				  </td>
				  </tr>
            <?php 
            $no++; 
            } 
              } else { 
            echo "0 results"; 
              } $koneksi->close(); 
            ?>
								
							</tbody>
									</table>
									<script type ="text/javascript" src="assets/js/jquery.main.js"></script>
									<script type ="text/javascript" src="assets/js/jquery.dataTables.main.js"></script>
									<script>$(document).ready(function(){
									$('#example').DataTables();
									});
									</script>
								</div>
                </section>

                <!-- Main content -->
                
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>     

    </body>
</html>